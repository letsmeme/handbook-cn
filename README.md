# Let's MeMe

Let's MeMe——The world’s first **Traffic-Fi** based social layer 2 network

\
Let's meme是世界上第一个基于Traffic-Fi协议的社交二层网络应用。

我们基于Twitter的原生社交网络，推出了一款Chrome Extension（即推特插件）。通过整合社交内容、社交图谱和web2社交媒体的流量价值，创建一个新的基于社区的社交网络。该产品主要为社区提供数据洞察、任务系统、热度榜单等功能帮助Web3原生社区更好的管理社区以及流量营销。

不久的将来，Let's meme还将在此基础上推出Traffic protocol，该协议将彻底改变web3的流量营销范式，让流量标准化、透明化。

<figure><img src=".gitbook/assets/image.png" alt=""><figcaption><p>项目主页 http://t.xyz</p></figcaption></figure>
