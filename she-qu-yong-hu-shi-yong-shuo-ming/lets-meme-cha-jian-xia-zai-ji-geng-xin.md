# Let's MeMe插件下载及更新

目前Let's MeMe插件的下载与安装，都可在Chrome商城进行。

Chrome商城进入方式：&#x20;

1. 使用谷歌浏览器的可以点击右上角的拓展程序，选择管理拓展程序。在管理拓展程序页面，点击主菜单，查看并点击最下方的打开Chrome商城即可进入商城。
2. 直接谷歌搜索：Chrome商城 进入商城后，搜索：Let's meme，点击进行下载与安装

如需更新，则在管理拓展程序页面，进行插件的移除，移除后，去搜索并下载安装最新版本即可
